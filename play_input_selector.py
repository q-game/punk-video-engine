#! ../env/bin/python
# encoding: utf-8
from os import path
import signal
import sys

from gi.repository import Gst

from utils import Element, Pipeline, Bin
from utils import link, flags, msg_str
from utils import qdict
#from utils import debug_graph
#debug_graph.init()

class Video(object):
    def __init__(self, file_path, output, id=''):
        self.name = path.basename(file_path).split('.')[0]
        self.video_out = None

        self.bin = Bin(self.name+id)
        self.el_src = Element('filesrc', 'src', location=file_path)
        self.el_decode = Element('decodebin', 'decoder')
        self.el_queue = Element('queue', 'queue')
        self.el_queue.src = self.el_queue.get_static_pad('src')
        self.el_queue.sink = self.el_queue.get_static_pad('sink')

        self.bin.add(self.el_src)
        self.bin.add(self.el_decode)
        self.bin.add(self.el_queue)
        link(self.el_src, self.el_decode)

        self.el_decode.connect('pad-added', self.pad_added, qdict({
            'output': output
        }))

    def pad_added(self, el, pad, data):
        if self.video_out is None:
            ghost_pad = Gst.GhostPad.new('src', self.el_queue.src)
            ghost_pad.set_active(True)
            self.bin.add_pad(ghost_pad)
            self.video_out = ghost_pad

            link(pad, self.el_queue.sink)
            link(self.video_out, data.output)


def input_select():
    pipeline = Pipeline('test-pipeline')

    selector = Element('input-selector', 'selector', sync_streams=False)
    selector.input_1 = selector.get_request_pad('sink_%u')
    selector.input_2 = selector.get_request_pad('sink_%u')
    selector.input_3 = selector.get_request_pad('sink_%u')
    sink = Element('autovideosink', 'videosink')

    video1 = Video(u'../../videos/4_popadanie.avi', selector.input_1)
    video2 = Video(u'../../videos/5_promah.avi', selector.input_2)
    video3 = Video(u'../../videos/7_victory.avi', selector.input_3)

    pipeline.add(video1.bin)
    pipeline.add(video2.bin)
    pipeline.add(video3.bin)
    pipeline.add(selector)
    pipeline.add(sink)

    link(selector, sink)
    selector.set_property('active-pad', selector.input_3)

    #debug_graph(pipeline, 'pre')
    pipeline.set_state('playing')

    bus = pipeline.get_bus()
    terminate = False
    count = 1
    while not terminate:
        msg = bus.timed_pop_filtered(
            1000*Gst.MSECOND,
            flags('MessageType', *'error eos'.split())
        )
        success, current = pipeline.query_position(Gst.Format.TIME)
        if msg is None:
            continue
            print 'None'

        if msg.type & flags('MessageType', 'error'):
            pipeline.terminate = True
            print msg

        if msg.type & flags('MessageType', 'eos'):
            print count
            if count == 1:
                selector.set_property('active-pad', selector.input_2)
                print 'selector input 2'
                print 'promah'
            elif count == 2:
                selector.set_property('active-pad', selector.input_3)
                print 'selector input 2'
                print 'victory'
            elif count == 3:
                selector.set_property('active-pad', selector.input_2)
                print 'selector input 2'
                print 'promah'
            elif count == 4:
                selector.set_property('active-pad', selector.input_1)
                print 'selector input 1'
                print 'popadanie'
            elif count == 5:
                selector.set_property('active-pad', selector.input_3)
                print 'selector input 3'
                print 'victory'
            elif count == 6:
                selector.set_property('active-pad', selector.input_3)
                print 'selector input 3'
                print 'victory'
            else:
                print 'terminate from else'
                terminate = True
            count += 1
            seek = pipeline.seek_simple(
                Gst.Format.TIME,
                flags('SeekFlags', 'flush', 'key_unit'),
                0*Gst.SECOND
            )
            print seek

    pipeline.set_state('null')


def main():
    Gst.init(sys.argv)
    input_select()


if __name__ == "__main__":
    main()

