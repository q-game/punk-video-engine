#!/bin/bash
# Удаляем старое виртуальное окружение, устанавливаем новое, накладываем патчи

cd `dirname "$0"`;
ENV_PATH='../../env/';

sudo apt-get install libzmq3-dev
sudo apt-get install python-dev
sduo apt-get install python-pip python-virtualenv
sudo apt-get install libffi-dev
sudo apt-get install libva1 libva-intel-vaapi-driver vainfo
sudo apt-get install gstreamer1.0-vaapi
sudo apt-get install unclutter


if [ $1 ]; then
    rm -r "$ENV_PATH";
    virtualenv --system-site-packages --python="python$1" "$ENV_PATH";
    $ENV_PATH/bin/pip install -r env.req;
else
   echo "Error: python version requires as first argument" 1>&2;
fi
