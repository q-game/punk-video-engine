#! ../env/bin/python
# encoding: utf-8
from os import path
import signal
import sys

import zmq
import json
from collections import deque
from vid_settings import ADRS

from gi.repository import Gst

from utils import Element, Pipeline, Bin
from utils import link, flags, msg_str
from utils import qdict
#from utils import debug_graph
#debug_graph.init()

zmq_ctx = zmq.Context()

class Video(object):
    def __init__(self, file_path, output, id=''):
        self.name = path.basename(file_path).split('.')[0]
        self.video_out = None

        self.bin = Bin(self.name+id)
        self.el_src = Element('filesrc', 'src', location=file_path)
        self.el_decode = Element('decodebin', 'decoder')
        self.el_queue = Element('queue', 'queue')
        self.el_queue.src = self.el_queue.get_static_pad('src')
        self.el_queue.sink = self.el_queue.get_static_pad('sink')

        self.bin.add(self.el_src)
        self.bin.add(self.el_decode)
        self.bin.add(self.el_queue)
        link(self.el_src, self.el_decode)

        self.el_decode.connect('pad-added', self.pad_added, qdict({
            'output': output
        }))

    def pad_added(self, el, pad, data):
        if self.video_out is None:
            ghost_pad = Gst.GhostPad.new('src', self.el_queue.src)
            ghost_pad.set_active(True)
            self.bin.add_pad(ghost_pad)
            self.video_out = ghost_pad

            link(pad, self.el_queue.sink)
            link(self.video_out, data.output)


def seeker():

    sub_sock = zmq_ctx.socket(zmq.SUB)
    sub_sock.setsockopt(zmq.SUBSCRIBE, "")
    address = "tcp://{ip}:{port}".format(**ADRS['VIDEO_PUB'])
    sub_sock.connect(address)

    pipeline = Pipeline('test-pipeline')

    #sink = Element('autovideosink', 'videosink')
    sink = Element('vaapisink', 'videosink', fullscreen=True)
    output = sink.get_static_pad('sink')

    video1 = Video(u'../../videos/lodka.mp4', output)

    pipeline.add(video1.bin)
    pipeline.add(sink)

    #debug_graph(pipeline, 'pre')
    pipeline.set_state('playing')

    vids = {
        'oblaka': (0, 29),
        'luk': (31, 59),
        'vspliv': (60, 89),
        'popal': (90, 114),
        'mimo': (115, 134),
        'otvet': (137, 188),
        'win': (191, None),
    }
    #vid_queue = deque(['oblaka', 'luk', 'vspliv', 'popal', 'mimo', 'popal', 'otvet', 'FLUSH', 'popal', 'win'])
    vid_queue = deque(['oblaka'])

    def next_vid(vid_queue):
        if len(vid_queue):
            vid_name = vid_queue.popleft()
            start, end = vids[vid_name]
            return True, start, end
        else:
            return False, None, None

    def flush_vid(vid_queue):
        if not len(vid_queue):
            return False
        else:
            if 'FLUSH' not in vid_queue:
                return False
            else:
                while True:
                    vid_name = vid_queue.popleft()
                    if vid_name == 'FLUSH':
                        print 'FLUSH'
                        #vid_name = vid_queue.popleft()
                        #start, end = vids[vid_name]
                        return True

    START, END = 0, 0

    bus = pipeline.get_bus()
    terminate = False
    while not terminate:
        msg = bus.timed_pop_filtered(
            100*Gst.MSECOND,
            flags('MessageType', *'error eos'.split())
            #flags('MessageType', ('any'))
        )

        if msg is not None:
            if msg.type & flags('MessageType', 'error'):
                pipeline.terminate = True
                print msg
                continue

            if msg.type & flags('MessageType', 'eos'):
                pipeline.terminate = True
                print msg
                continue

        try:
            request = json.loads(sub_sock.recv_string(zmq.NOBLOCK))
        except zmq.Again as e:
            pass
        else:
            #TODO: change vid_queue
            if request.get('type') == 'queue':
                vid_queue.extend(request.get('data'))
            elif request.get('type') == 'system':
                if request.get('data') == 'stop':
                    break


        success, current = pipeline.query_position(Gst.Format.TIME)
        print success, current
        print current/float(Gst.SECOND)
        need_flush = flush_vid(vid_queue)
        if need_flush:
            print "need_flush"
            END = -1*Gst.SECOND
            START = 1000000*Gst.SECOND

        if END is None:
            continue


        if (current > END*Gst.SECOND) or (current < START*Gst.SECOND):
            have_vid, start, end = next_vid(vid_queue)
            if have_vid:
                print "have_vid: {0} - {1}".format(start, end)
                START = start
                END = end

            seek = pipeline.seek_simple(
                Gst.Format.TIME,
                flags('SeekFlags', 'flush', 'key_unit'),
                START*Gst.SECOND
            )
            print seek

    pipeline.set_state('null')


def main():
    Gst.init(sys.argv)
    seeker()


if __name__ == "__main__":
    main()

